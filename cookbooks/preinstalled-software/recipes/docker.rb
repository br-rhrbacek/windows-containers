docker_version = node[:docker][:docker_version]
nuget_package_provider_version = node[:docker][:nuget_package_provider_version]
return unless docker_version && docker_version != ''

powershell_script "Install version #{nuget_package_provider_version} NuGet Powershell package provider" do
  code "Install-PackageProvider -Name NuGet -MinimumVersion #{nuget_package_provider_version} -Force"
end

# Install Docker by following steps in https://cloud.google.com/compute/docs/containers/#docker_on_windows

powershell_script 'Install Docker module' do
  code 'Install-Module -Name DockerMsftProvider -Repository PSGallery -Force -Confirm:$false'
end

powershell_script "Install version #{docker_version} of Docker package" do
  code 'Install-Package -Name docker -ProviderName DockerMsftProvider ' \
    "-Force -RequiredVersion #{docker_version}"
end

powershell_script 'Disable Receive Segment Coalescing' do
  # Disable receive segment coalescing is required to fix a known issue with
  # Docker 19.03.5, where internet access in a docker NAT network is slow.
  # See https://github.com/docker/for-win/issues/698 for more info
  code 'netsh netkvm setparam 0 *RscIPv4 0'
end

powershell_script 'Enable IPv6' do
  # Enabling IPv6 is required to fix a known issue with Docker 19.03.5
  # on Windows Server 2016 instances with Windows Update KB4015217 installed.
  # See https://github.com/moby/moby/issues/32595 for more info
  code 'reg add HKLM\SYSTEM\CurrentControlSet\Services\Tcpip6\Parameters ' \
    '/v DisabledComponents /t REG_DWORD /d 0x0 /f'
end
