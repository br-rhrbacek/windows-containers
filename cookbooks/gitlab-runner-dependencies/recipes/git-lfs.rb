directory 'Create git-lfs root directory' do
  path node[:git_lfs][:root_path]
  action :create
end

remote_file 'Download git-lfs from remote' do
  path node[:git_lfs][:zip_path]
  source "https://github.com/git-lfs/git-lfs/releases/download/#{node[:git_lfs][:version]}/git-lfs-windows-amd64-#{node[:git_lfs][:version]}.zip"
  checksum node[:git_lfs][:checksum]
  action :create
end

powershell_script 'Extract git-lfs' do
  code "Expand-Archive -Path #{node[:git_lfs][:zip_path]} -DestinationPath #{node[:git_lfs][:root_path]}"
  cwd node[:git_lfs][:root_path]
end

file 'Delete zip archive' do
  path node[:git_lfs][:zip_path]
  action :delete
end

windows_path 'Add git-lfs to path' do
  path node[:git_lfs][:root_path]
  action :add
end

powershell_script 'Install git-lfs hooks' do
  code "git-lfs install --skip-repo"
end
